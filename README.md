# SmartWall PHP SDK
Find the documentation here: https://doc.smartwall.ai


Import it in your PHP project using Composer

> Warning: This package is not installable via Composer 1.x, please make 
sure you upgrade to Composer 2+.

Add this to your composer.json file.

```json
  "require": {
    "smartwall/sw-php-sdk": "@stable"
  }

```


## Development

To deploy a new version of PHP SDK use git tag with the new version number and push
changes to master branch. It will automatically publisher in [Packagist](https://packagist.org/). 