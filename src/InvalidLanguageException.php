<?php

namespace SmartWall\SDK;

use Exception;

class InvalidLanguageException extends Exception
{
    function __construct(string $languageCode) {
        parent::__construct("Invalid language code=" . $languageCode);

    }
}