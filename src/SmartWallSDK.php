<?php

namespace SmartWall\SDK;

use SmartWall\SDK\api\SmartWallAPI;

class SmartWallSDK
{
    /** @var string[] */
    const ALLOWED_LANGUAGES = ['fr', 'en', 'de', 'pt', 'it', 'es', 'ja', 'ms'];

    /** @var Environment */
    private $environment;
    /** @var SmartWallAPI */
    private $api;
    /** @var string */
    private $smartwallUid;
    /** @var string */
    private $languageCode;
    /** @var string */
    private $userUid;
    /** @var string */
    private $articleUid;

    /**
     * SDK constructor.
     * @param string $smartwallUid
     * @param string $apiKey
     * @param string $articleUid
     * @param string $languageCode
     * @param string $environment
     * @param string $userUid
     * @throws InvalidLanguageException
     */
    function __construct(string $smartwallUid,
                         string $apiKey,
                         string $articleUid,
                         string $languageCode = 'en',
                         string $environment = 'production',
                         string $userUid = '')
    {
        $this->checkLanguage($languageCode);
        $this->smartwallUid = $smartwallUid;
        $this->languageCode = $languageCode;
        $this->articleUid = $articleUid;
        $this->environment = new Environment($environment);
        $this->api = new SmartWallAPI($apiKey, $this->environment);
        $cookieHandler = new CookieHandler();
        $this->userUid = $userUid ?: ($cookieHandler->get('sw-user-uid') ?? "");
    }

    /**
     * Returns true if the user has access to the article.
     * Can be use to decide if to send full article content or not.
     * Not recommended to be used if your website pages are being cached.
     * @param Article article
     * @return boolean
     */
    function hasAccess(Article $article) {
        if ($this->userUid) {
            return $this->api->hasAccess($article, $this->smartwallUid, $this->userUid);
        }
        return false;
    }

    /**
     * @param string $articleSelector
     * @param array $customVariables
     * @return string
     */
    function initScripts(string $articleSelector, array $customVariables = []): string
    {
        if ($this->environment->isProduction()) {
            $environmentName = '';
        } else {
            $environmentName = $this->environment->getEnvironmentName();
        }
        $apiKey = $this->api->getApiKey();
        $customVariablesJson = json_encode($customVariables, JSON_FORCE_OBJECT);
        $scripts = "<script src=" . $this->environment->getScriptsUrl() . "></script>";
        $scripts .= "<script>
            (function() {
              console.info('Init JS SDK');
              window.smartWallSDK = new SmartWallSDK({
                articleUid: '$this->articleUid',
                smartwallUid: '$this->smartwallUid',
                apiKey: '$apiKey',
                environment: '$environmentName',
                mode: 'backend',
                language: '$this->languageCode',
                articleSelector: '$articleSelector',
                customVariables: $customVariablesJson
              })
            })();
        </script>";
        return $scripts;
    }

    public function initArticle(string $articleContent): Article
    {
        $articleUrl = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        $article = new Article($this->articleUid, $this->smartwallUid, $articleUrl, $articleContent);
        if (!$this->api->isArticleCached($article)) {
            $this->api->createArticle($article);
        }
        return $article;
    }

    /**
     * @param string $languageCode
     * @throws InvalidLanguageException
     */
    private function checkLanguage(string $languageCode)
    {
        if (in_array($languageCode, self::ALLOWED_LANGUAGES)) {
            return;
        }
        throw new InvalidLanguageException($languageCode);
    }
}