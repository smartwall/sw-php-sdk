<?php

namespace SmartWall\SDK\api;

class Response
{

    private $statusCode;
    private $responseBody;
    /** @var string */
    private $hash;
    /** @var string */
    private $requestMethod;
    private $info;

    /**
     * Response constructor.
     * @param $statusCode
     * @param $responseBody
     * @param string $hash
     * @param string $requestMethod
     * @param $info
     */
    public function __construct($statusCode, $responseBody, string $hash, string $requestMethod, $info)
    {
        $this->statusCode = $statusCode;
        $this->responseBody = $responseBody;
        $this->hash = $hash;
        $this->requestMethod = $requestMethod;
        $this->info = $info;
    }


    /**
     * @return bool Returns true if response has a success http code ( 200 >= code < 400).
     */
    public function isSuccessful(): bool
    {
        return $this->statusCode >= 200 && $this->statusCode < 400;
    }

    /**
     * @return bool inverted result of isSuccessful()
     */
    public function isFailed(): bool
    {
        return !$this->isSuccessful();
    }

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @return mixed
     */
    public function getResponseBody()
    {
        return $this->responseBody;
    }

    /**
     * @return string
     */
    public function getHash(): string
    {
        return $this->hash;
    }

    /**
     * @return string
     */
    public function getRequestMethod(): string
    {
        return $this->requestMethod;
    }

    /**
     * @return mixed
     */
    public function getInfo()
    {
        return $this->info;
    }
}