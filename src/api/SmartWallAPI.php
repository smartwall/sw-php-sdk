<?php

namespace SmartWall\SDK\api;

use SmartWall\SDK\Article;
use SmartWall\SDK\Environment;
use SmartWall\SDK\Logger;

class SmartWallAPI
{
    /** @var Environment */
    private $environment;
    /** @var string */
    private $apiKey;
    /** @var Request */
    private $request;
    /** @var ResponseLogger */
    private $responseLogger;

    function __construct(string $apiKey, Environment $environment)
    {
        $this->apiKey = $apiKey;
        $this->environment = $environment;
        $this->request = new Request($environment);
        $this->responseLogger = new ResponseLogger();
        $this->logger = new Logger($environment);
    }

    public function hasAccess(Article $article, string $smartwallUid, string $userUid) {
        $fullEndpointPath = '/v2/access?articleUid=' . $article->getUid() . 
        '&smartwallUid=' . $smartwallUid  . '&userUid=' . $userUid 
        . '&apiKey=' . $this->apiKey;
        $response = $this->runGetRequest($fullEndpointPath);
        if ($response->isSuccessful()) {
            return $response->getResponseBody()->access;
        } else {
            return false;
        }
    }

    /**
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->apiKey;
    }

    /**
     * @param Article $article
     * @param string $smartwallUid
     * @return Response The response after a call to the article's CREATE endpoint.
     */
    public function createArticle(Article $article): Response
    {
        $body = [
            'apiKey' => $this->apiKey,
            'smartwallUid' => $article->getSmartwallUid() ,
            'articleUid' => $article->getUid(),
            'content' => $article->getContent(),
            'url' => $article->getUrl()
        ];
        return $this->runPostRequest('/v2/articles/', $body);
    }

    /**
     * @param Article $article
     * @return true if is already cached
     */
    public function isArticleCached(Article $article): bool
    {
        $hash = md5($article->getContent());
        $fullEndpointPath = '/v2/articles/' . $article->getUid() . '/is-cached' 
        . '?smartwallUid=' . $article->getSmartwallUid() 
        . '&md5=' . $hash 
        . '&apiKey=' . $this->apiKey;
        $response = $this->runGetRequest($fullEndpointPath);
        return $response->isSuccessful() && $response->getResponseBody()->status;
    }

    private function runPostRequest(string $endpoint, array $body): Response
    {
        return $this->runRequest(Request::METHOD_POST, $endpoint, $body);
    }

    private function runGetRequest(string $endpoint): Response
    {
        return $this->runRequest(Request::METHOD_GET, $endpoint, []);
    }

    private function runRequest(string $method, string $endpoint, array $body): Response
    {
        $response = $this->request->exec($method, $endpoint, $body);
        $this->responseLogger->writeIfFailed($response);
        $this->logger->print('Request to SW API '.$response->getHash(), array(
            "Method"=>$method, 
            "Endpoint path"=>$endpoint, 
            "Request body"=>$body,
            "Status code"=>$response->getStatusCode(),
            "Response body"=> $response->getResponseBody(),
            "Info"=> $response->getInfo()
        ));
        return $response;
    }

}