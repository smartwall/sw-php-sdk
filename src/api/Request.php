<?php

namespace SmartWall\SDK\api;

use SmartWall\SDK\Environment;

class Request
{
    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';

    /** @var Environment */
    private $environment;

    /**
     * @param Environment $environment
     */
    public function __construct(Environment $environment)
    {

        $this->environment = $environment;
    }

    /**
     * HTTP request
     * @param string $method
     * @param string $endpoint The path after the base URL i.e.: /v2/render-wall
     * @param array $body
     * @return Response
     */
    public function exec(string $method, string $endpoint, array $body): Response
    {
        $requestUrl = $this->buildRequestUrl($endpoint);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $requestUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

        if ($method !== self::METHOD_GET) {
            $this->setHeadersAndBody($ch, $body);
        }

        $verbose = fopen('php://temp', 'w+');
        if ($this->environment->isDev()) {
            // Use http
            curl_setopt($ch, CURLOPT_URL, str_replace('https', 'http', $requestUrl));
            // to access local dev certificate
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            // change port locally
            curl_setopt($ch, CURLOPT_PORT, 80);
            // for debug purpose
            curl_setopt($ch, CURLOPT_VERBOSE, true);
            curl_setopt($ch, CURLOPT_STDERR, $verbose);
            // for redirect requests
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        }
        $result_clean = curl_exec($ch);

        $responseBody = json_decode($result_clean);
        $info = curl_getinfo($ch);
        $statusCode = $info['http_code'];
        $hash = $this->buildHash($method, $requestUrl);
        http_response_code($statusCode);
        curl_close($ch);
        return new Response($statusCode, $responseBody, $hash, $method, $info);
    }

    /**
     * @param $curlInit resource|false a cURL handle on success, false on errors. From call to curl_init().
     * @param array $body
     */
    private function setHeadersAndBody($curlInit, array $body)
    {
        $json_body = json_encode($body);
        $headers = array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($json_body),
        );
        curl_setopt($curlInit, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curlInit, CURLOPT_POSTFIELDS, $json_body);
    }

    /**
     * @param string $endpoint  The path after the base URL i.e.: /v2/render-wall
     * @return string
     */
    private function buildRequestUrl(string $endpoint): string
    {
        return $this->environment->getApiBaseUrl() . $endpoint;
    }

    /**
     * @param string $method
     * @param string $requestUrl
     * @return string
     */
    private function buildHash(string $method, string $requestUrl): string
    {
        return md5($method . $requestUrl . $this->environment->getEnvironmentName() . date(DATE_RFC2822));
    }


}
