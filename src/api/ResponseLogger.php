<?php

namespace SmartWall\SDK\api;

class ResponseLogger
{

    /**
     * @param Response $response writes info about response to stderr if status code is not 200
     */
    public function writeIfFailed(Response $response)
    {
        if ($response->isFailed()) {
            $data = [$response->getRequestMethod(), $response->getInfo(), $response->getResponseBody()];
            self::write($response->getHash() . " Failed request:\n", $data);
        }
    }

    /**
     * Writes messages and data to the log
     * @param string $message
     * @param array $data
     */
    private static function write(string $message, array $data = [])
    {
        $fh = fopen('php://stderr', 'a'); //both (a)ppending, and (w)riting will work
        $newLine = $message;
        if (!empty($data)) {
            $newLine .= ' ' . print_r($data, true);
        }
        fwrite($fh, $newLine . PHP_EOL);
        fclose($fh);
    }
}
