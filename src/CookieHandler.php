<?php

namespace SmartWall\SDK;

class CookieHandler
{
    public $expire = 0;
    public $path = '/';
    public $domain = 'smartwall.ai';
    public $name;
    public $value;

    function get($name, $default = null)
    {
        if (isset($_COOKIE[$name])) {
            $this->name = $name;
            $this->value = $_COOKIE[$name];
            return $this->value;
        }
        return $default;
    }

    function set($name, $value)
    {
        $this->name = $name;
        $this->value = $value;
        setcookie($name, $value, $this->expire, $this->path, $this->domain, true);
    }

    /**
     * Get JSON stored in cookie as an object.
     * @param $name
     * @return mixed
     */
    function getJSON($name)
    {
        return json_decode($this->get($name, []));
    }

    /**
     * Get pair key value from a cookie that is storing a JSON.
     * @param string $name
     * @param string $key
     * @param null $default
     * @return mixed|null
     */
    function getFromJSON(string $name, string $key, $default = null)
    {
        $obj = $this->getJSON($name);
        if (isset($obj[$key])) {
            return $obj[$key];
        }
        return $default;
    }

    /**
     * Set pair key value in a cookie as JSON.
     * @param string $name
     * @param string $key
     * @param $value
     */
    function setInJSON(string $name, string $key, $value)
    {
        $obj = $this->getJSON($name);
        $obj[$key] = $value;
        $this->set($name, json_encode($obj));
    }
}

?>
