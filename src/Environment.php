<?php

namespace SmartWall\SDK;

/**
 * Class Environment
 * Holds information (url and initial environment string) about the current environment.
 */
class Environment
{
    /** @var string */
    private $environmentName;

    /**
     * Environment constructor.
     * @param string $environmentName 'dev' | 'production' | null
     */
    public function __construct(string $environmentName)
    {
        $this->environmentName = $environmentName;
    }

    /**
     * @return string Returns the base URL for api endpoints. i.e.: 'https://api.dev.smartwall.ai'
     */
    public function getApiBaseUrl(): string
    {
        return $this->getHostBaseUrl('api');
    }

    /**
     * @return string Returns the URL for javascript CDN. i.e.: 'https://sdk.dev.smartwall.ai/?v=2'
     */
    public function getScriptsUrl(): string
    {
        $baseScriptUrl = $this->getHostBaseUrl('sdk');
        $debugParameter = $this->isDebugging() ? '&debug' : '';
        return $baseScriptUrl . '/?v=2' . $debugParameter;
    }

    private function getHostBaseUrl(string $subdomain): string
    {
        $strEnv = $this->isProduction() ? '' : $this->environmentName . '.';
        return 'https://' . $subdomain . '.' . $strEnv . 'smartwall.ai';
    }


    public function isProduction(): bool
    {
        return $this->environmentName === 'production';
    }

    public function isDev(): bool
    {
        return $this->environmentName === 'dev';
    }

    /**
     * @return bool True if demo site is configured in debug mode
     */
    public function isDebugging(): bool
    {
        return isset($_COOKIE['sw-debug']) && $_COOKIE['sw-debug'] == '1';
    }

    /**
     * @return string
     */
    public function getEnvironmentName(): string
    {
        return $this->environmentName;
    }

}
