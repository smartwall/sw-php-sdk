<?php

namespace SmartWall\SDK;

class Wall
{

    /** @var string */
    private $template;
    /** @var string */
    private $customCss;
    /** @var Article */
    private $article;

    /**
     * Wall constructor.
     * @param string $template
     * @param string $customCss
     */
    public function __construct(string $template, string $customCss)
    {
        $this->template = $template;
        $this->customCss = $customCss;
    }

    /**
     * @return string
     */
    public function getTemplate(): string
    {
        return $this->template;
    }

    /**
     * @return string
     */
    public function getCustomCss(): string
    {
        return $this->customCss;
    }

    /**
     * @return Article
     */
    public function getArticle(): Article
    {
        return $this->article;
    }

    /**
     * @param Article $article
     */
    public function setArticle(Article $article)
    {
        $this->article = $article;
    }

}