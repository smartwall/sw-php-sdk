<?php

namespace SmartWall\SDK;

use Urodoz\Truncate\TruncateService;

class Article
{
    /** @var string */
    private $uid;
    /** @var string */
    private $smartwallUid;
    /** @var string */
    private $url;
    /** @var string */
    private $content;


    /**
     * Article constructor.
     * @param string $uid
     * @param string $url
     * @param string $content
     */
    public function __construct(string $uid, string $smartwallUid, string $url, string $content)
    {
        $this->uid = $uid;
        $this->smartwallUid = $smartwallUid;
        $this->url = $url;
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getUid(): string
    {
        return $this->uid;
    }

    /**
     * @return string
     */
    public function getSmartwallUid(): string
    {
        return $this->smartwallUid;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    public function getCutContent(int $numberCharactersPreview = 500): string
    {
        $truncateService = new TruncateService();
        return $truncateService->truncate($this->content, $numberCharactersPreview);
    }

}