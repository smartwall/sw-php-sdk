<?php

namespace SmartWall\SDK;

class Logger
{
  public $debugging = 0;

  function __construct(Environment $environment)
  {
    if ($environment->isDebugging()) {
      $this->debugging = 1;
    }
  }

  /**
   * Print logs in stdout
   * @param string $msg Message to print
   * @param any[] $data Data to print
   */
  function print($msg, $data = [])
  {
    if ($this->debugging == 0) {   
      return;
    }
    $stdout = fopen('php://stdout', 'w');
    $dateTime = date(DATE_RFC2822);
    $newLine = $dateTime . ' ' . $msg;
    if (!empty($data)) {
      $newLine = $newLine . ' ' . print_r($data, true);
    }
    fwrite($stdout, $newLine . PHP_EOL);
  }
}
