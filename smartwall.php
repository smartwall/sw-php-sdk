<?php

/**
 * LEGACY FILE!
 *
 * Left here for temporary compatibility with V1
 */

if ( is_session_started() === FALSE ) session_start();
checkMissingExtenssion();

CONST USER_ID_TOKEN = 'sw_php_sdk_user_id_token';
CONST USER_ID = 'sw_php_sdk_user_id';
CONST METRICS_TOKEN_NAME = 'sw_php_sdk_metrics_token';

$uniqId = uniqid('php-', true);
// Because of the headers beeing sent we must set cookies before anything else

$debugCookiesInfo = array('isset($_COOKIE[USER_ID_TOKEN])'=>isset($_COOKIE[USER_ID_TOKEN]), 'isset($_SESSION[USER_ID_TOKEN])'=>isset($_SESSION[USER_ID_TOKEN]));

// do we have a cookie ?
if (isset($_COOKIE[USER_ID_TOKEN])) {
    $token = explode('.', $_COOKIE[USER_ID_TOKEN]);
    $tokenData = SmartWall::jsonDecode(SmartWall::urlsafeB64Decode($token[1]));

    $tmpUserId = isset($tokenData->userId) ? $tokenData->userId : $tokenData->globalUserId; // TODO remove globalUserId
    $_SESSION[USER_ID] = $tmpUserId;
    setcookie(USER_ID, $tmpUserId, time() + 31556926, '/'); // one year

} else if ( isset($_SESSION[USER_ID_TOKEN]) ) { // do we have a session ?
    $token = explode('.', $_SESSION[USER_ID_TOKEN]);
    $tokenData = SmartWall::jsonDecode(SmartWall::urlsafeB64Decode($token[1]));

    $tmpUserId = isset($tokenData->userId) ? $tokenData->userId : $tokenData->globalUserId; // TODO remove globalUserId
    $_SESSION[USER_ID] = $tmpUserId;
    setcookie(USER_ID,  $tmpUserId, time() + 31556926, '/'); // one year
    setcookie(USER_ID_TOKEN, $_SESSION[USER_ID_TOKEN], time() + 31556926, '/'); // one year

} else { // we have nothing we create it
    $_SESSION[USER_ID] = $uniqId;
    setcookie(USER_ID, $uniqId, time() + 31556926, '/'); // one year
}

if (!isset($_COOKIE[METRICS_TOKEN_NAME]) && isset($_SESSION[METRICS_TOKEN_NAME])) {
    setcookie(METRICS_TOKEN_NAME, $_SESSION[METRICS_TOKEN_NAME], time() + 31556926, '/'); // one year
}

class SmartWallMetric {
    private $metricsTokenName = METRICS_TOKEN_NAME;
    private $environment = '';
    private $name = '';
    private $value = 1;

    function __construct($opts = null) {
        if ($opts !== null) {
            $this->name = isset($opts['name']) ? $opts['name'] : null;
            $this->value = isset($opts['value']) ? $opts['value'] : null;
            $this->environment = isset($opts['environment']) ? $opts['environment'] . '.' : null;
        }

        if(empty($this->name)) {
            throw new ErrorException('You must provide a name');
        }

        // send the metric
        $this->sendCustomMetric();
    }

    private function sendCustomMetric() {
        $endpoint = 'https://api.' . $this->environment . 'smartwall.ai/v1/smart-walls/custom-metric';

        $dataJson = array(
            'name' => $this->name,
            'value' => $this->value,
            'metricsToken' => isset($_SESSION[$this->metricsTokenName]) ? $_SESSION[$this->metricsTokenName] : $_COOKIE[$this->metricsTokenName] ,
        );

        $dataJson = json_encode($dataJson);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');

        $headers = array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($dataJson),
        );

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataJson);

        $verbose = fopen('php://temp', 'w+');
        if ($this->environment === 'dev.') {
            // Use http
            curl_setopt($ch, CURLOPT_URL, str_replace('https', 'http', $endpoint));
            // to access local dev certificate
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            // change port locally
            curl_setopt($ch, CURLOPT_PORT, 80);
            // for debug purpose
            curl_setopt($ch, CURLOPT_VERBOSE, true);
            curl_setopt($ch, CURLOPT_STDERR, $verbose);
        }

        $resultClean = curl_exec($ch);
        $result = json_decode($resultClean);

        $info = curl_getinfo($ch);
        http_response_code($info['http_code']);

        curl_close($ch);
        return $result;
    }
}

class SmartWall {
    private $idArticleSelector = 'sw-php-sdk-article';
    private $metricsTokenName = METRICS_TOKEN_NAME;
    private $environment = '';
    private $language = 'en';
    private $demoToken;
    private $smartWallId;
    private $articleId;
    private $userId;
    private $endpoint;
    private $customVariables;
    private $secretKey;
    private $requestedWith;
    private $apiRes = [];
    private $debug;

    function __construct($opts = null) {
        if ($opts !== null) {
            $this->smartWallId = isset($opts['smartWallId']) ? $opts['smartWallId'] : null;
            $this->language = isset($opts['language']) ? $opts['language'] : null;
            $this->articleId = isset($opts['articleId']) ? md5($opts['articleId']) : null;
            $this->userId = isset($opts['userId']) ? $opts['userId'] : null;
            $this->secretKey = isset($opts['secretKey']) ? $opts['secretKey'] : null;
            $this->debug = isset($opts['debug']) ? $opts['debug'] : null;
            $this->customVariables = isset($opts['customVariables']) ? $opts['customVariables'] : null;
            $this->environment = isset($opts['environment']) ? $opts['environment'] . '.' : null;
            $this->demoToken = isset($opts['demoToken']) ? $opts['demoToken'] : null;
            $this->requestedWith = isset($opts['requestedWith']) ? $opts['requestedWith'] : null;
            
            if ($this->debug) {
                $this->printLogs('Starting debug session with settings: ', $this);
            }
        }
    }

    private function getBaseUrl() {
        return 'https://api.' . $this->environment . 'smartwall.ai/v1/';
    }

    public function __set($property, $value) {
        if ($this->debug) { $this->printLogs('Setting property '.$property.' = ', $value); }
        $allowedToChange = ['demoToken', 'language', 'smartWallId', 'articleId', 'userId', 'environment', 'endpoint', 'secretKey', 'debug', 'customVariables', 'logsFileName', 'requestedWith'];
        if (property_exists($this, $property) && in_array($property, $allowedToChange)) {
            if ($property === 'environment') {
                $value .= '.';
            }
            if ($property === 'articleId') {
                $value = md5($value);
            }
            $this->$property = $value;
        } else {
            $this->throwError('You cannot change/access the property: ' . $property);
        }

        return $this;
    }

    // show the smartWall
    public function show($htmlContent, $unlocked = false) {
        global $debugCookiesInfo;
        if ($this->debug) { $this->printLogs('Before all, handling cookies', $debugCookiesInfo); }
        if ($this->debug) { $this->printLogs('Showing the smartwall...'); }

        $userId = isset($_COOKIE[USER_ID]) ? $_COOKIE[USER_ID] : $_SESSION[USER_ID];

        if(empty($this->articleId)) {
            $this->throwError('You must provide an article ID');
        }

        if(empty($this->secretKey)) {
            $this->throwError('You must provide a secret Key');
        }

        if(empty($this->userId)) {
            $this->userId = $userId;
        }

        // encrypt articleContent
        $realKey = md5($this->secretKey . $this->articleId . $userId);
        $encryptedContent = cryptoJsAesEncrypt($realKey, $htmlContent);

        $apiRes = $this->callShowEndpoint(strlen($htmlContent));

        // if we have an access granted, we return the article
        if ($unlocked === true || (isset($apiRes->accessGranted) && $apiRes->accessGranted === true)) {
            if ($this->debug) { $this->printLogs('Access is granted, returning article.'); }
            return '<div id="' . $this->idArticleSelector . '" class="article-id-' . $this->articleId . '">' . $htmlContent . '</div>';
        }

        // if we have no wall to display
        if (isset($apiRes->noWall) && $apiRes->noWall === true) {
            return $htmlContent;
        }

        // check if we should truncate the content
        if (!empty($apiRes->threshold)) {
            $contentLength = strlen(strip_tags($htmlContent));

            $truncatedlength = ($contentLength * $apiRes->threshold) / 100;

            $htmlContent = $this->truncateHTML($htmlContent, $truncatedlength);
        }

        if ($this->debug) { $this->printLogs('Wrapping content and showing smartwall.'); }


        // return the content wrapped into our own element
        if(empty($apiRes->templates)) {
            $this->throwError('Error calling smartwall API show endpoint. Smartwall not retrieved.');
        }
        return '<div id="' . $this->idArticleSelector . '" class="article-id-' . $this->articleId . '">' . $htmlContent . '</div>' .
            $apiRes->templates->hideArea .
            $apiRes->templates->videoPlayer .
            $apiRes->templates->base .
            '<div id="sw-article-full-content" class="article-id-' . $this->articleId . '" style="display: none;">' . $encryptedContent . '</div>';
    }

    // show the necessary scripts with data coming from the backend
    public function getScripts($nArticleCharacters = null) {
        if ($this->debug) { $this->printLogs('Getting necessary scripts...'); }

        $endpoint = $this->getBaseUrl() . 'smart-walls/show';

        if (empty($this->apiRes[$endpoint]) && $nArticleCharacters === null) {
            $this->throwError('getScripts method must be call after show method or must be called with $nArticleCharacters as non-null');
        }

        $apiRes = clone $this->callShowEndpoint($nArticleCharacters);

        unset($apiRes->templates);

        $apiRes->articleSelector = '#' . $this->idArticleSelector;

        $articleIdClass = 'article-id-' . $this->articleId;

        return '<script src="https://sdk.' . $this->environment . 'smartwall.ai?v=1' . ($this->debug ? '&debug' : '') . '"></script>
                <script>
                var smartWallSdk = new SmartWallSDK({
                    mode: \'backend\',
                    language: \'' . $this->language . '\',
                    environment: \'' . str_replace('.', '', $this->environment) . '\',
                    userId: \'' . $this->userId . '\',
                    articleId: \'' . $this->articleId . '\',
                    endpoint: \'' . $this->endpoint . '\',
                    multiple: true,
                    selectorCSS: \'#' . $this->idArticleSelector . '.' . $articleIdClass . '\',
                    apiData: ' . json_encode($apiRes) . ',
                });
                </script>';
    }

    /**
    *  The method proxy all calls to the SmartWall API
    *  **/
    public function apiProxy() {
        if ($this->debug) { $this->printLogs('Proxying call to API...'); }

        if (isset($_COOKIE[USER_ID_TOKEN])) {
            $token = explode('.', $_COOKIE[USER_ID_TOKEN]);
            $tokenData = static::jsonDecode(static::urlsafeB64Decode($token[1]));
        }

        // assign articleId and userId
        if (isset($_POST['data'])) {
            $this->articleId = $_POST['data']['articleId'];
        } else {
            $this->articleId = $_POST['params']['articleId'];
        }

        $request = array(
            'endpoint' => isset($_POST['url']) ? $_POST['url'] : null,
            'method' => isset($_POST['method']) ? $_POST['method'] : null,
            'data' => isset($_POST['data']) ? $_POST['data'] : null,
            'params' => isset($_POST['params']) ? $_POST['params'] : null,
            'language' => isset($_POST['language']) ? $_POST['language'] : null,
            'userToken' => isset($_POST['userToken']) ? $_POST['userToken'] : null,
            'referrer' => isset($_POST['referrer']) ? $_POST['referrer'] : null
        );
        $apiRes = $this->callAPI($request);

        // if we have an acess granted we add the full article content to the answer
        if (isset($apiRes->accessGranted) && $apiRes->accessGranted === true) {
            $tmpUserId = isset($tokenData->userId) ? $tokenData->userId : $tokenData->globalUserId; // TODO remove globalUserId
            $apiRes->decryptionKey = md5($this->secretKey . $this->articleId .  $tmpUserId); 
        }

        if ($this->debug) {
            $this->printLogs('Proxy call request data', $request);
            $this->printLogs('Proxy call response', $apiRes);
        }

        echo json_encode($apiRes);
        die();
    }

    private function callShowEndpoint($nArticleCharacters) {
        if(empty($this->articleId)) {
            $this->throwError('You must provide an article ID');
        }

        if(empty($this->endpoint)) {
            $this->throwError('You must provide an endpoint');
        }

        if(empty($this->userId)) {
            $this->userId =  isset($_COOKIE[USER_ID]) ? $_COOKIE[USER_ID] :  $_SESSION[USER_ID];
        }

        $data = array(
            'smartWallId' => $this->smartWallId,
            'articleId' => $this->articleId,
            'userId' => $this->userId,
            'nArticleCharacters' => $nArticleCharacters,
            'publisherReferrer' => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '',
        );

        if ($this->customVariables !== null) {
            $data['customVariables'] = $this->customVariables;
        }

        // add the user ID anyway
        $data['userId'] =  $this->userId;

        if ($this->demoToken) {
            $data['demoToken'] = $this->demoToken;
        }

        return $this->callAPI(array(
            'endpoint' => $this->getBaseUrl() . 'smart-walls/show',
            'method' => 'POST',
            'cacheResponse' => true,
            'data' => $data
        ));
    }

    private function getRealUserIp(){
        switch(true){
            case (!empty($_SERVER['HTTP_X_REAL_IP'])) : return $_SERVER['HTTP_X_REAL_IP'];
            case (!empty($_SERVER['HTTP_CLIENT_IP'])) : return $_SERVER['HTTP_CLIENT_IP'];
            case (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) : return $_SERVER['HTTP_X_FORWARDED_FOR'];
            default : return $_SERVER['REMOTE_ADDR'];
        }
    }

    private function callAPI($opts) {
        if ($this->debug) { $this->printLogs('Call to API...'); }
        
        extract($opts);

        if ($this->debug) { $this->printLogs('Calling endpoint: '.$endpoint.(empty($query) ? '' : '?'.$query)); }

        if (isset($cacheResponse) && !empty($this->apiRes[$endpoint])) {
            if ($this->debug) { $this->printLogs('Response cached'); }
            return $this->apiRes[$endpoint];
        }

        $dataJson = NULL;
        if (!empty($data)) {
            // number must be a real integer
            if (isset($data['number'])) {
                $data['number'] = intval($data['number']);
            }

            if (isset($data['blockId'])) {
                $data['blockId'] = intval($data['blockId']);
            }

            $dataJson = json_encode($data);
        }
        if (isset($params)) {
            $query = http_build_query($params);
        }

        $ch = curl_init();
        $url = $endpoint;
        if (!empty($query)) {
            $url = "$endpoint?$query";
        }
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper($method));

        $referrerUrl = NULL;
        if (!empty($referrer)) {
            $referrerUrl = $referrer;
        } else {
            $referrerUrl = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        }

        // Most updated bearer token is stored in session, because session token is updated after every call to API
        $tmpBearerToken = isset($_SESSION[USER_ID_TOKEN]) ? $_SESSION[USER_ID_TOKEN] :  (isset($_COOKIE[USER_ID_TOKEN]) ? $_COOKIE[USER_ID_TOKEN] : '');
        $headers = array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($dataJson),
            'Referrer: ' . $referrerUrl,
            'user-agent: ' . $_SERVER['HTTP_USER_AGENT'],
            'accept-language: ' . $_SERVER['HTTP_ACCEPT_LANGUAGE'],
            'x-smartwall-language: ' . (!empty($language) ? $language : $this->language),
            'x-forwarded-for: ' . $this->getRealUserIp(),
            'authorization: Bearer ' . $tmpBearerToken,
        );

        if (isset($this->requestedWith)) {
            $headers['x-requested-with'] = $this->requestedWith;
        }

        if ($this->debug) { $this->printLogs('Headers: ', $headers); }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        if (!empty($dataJson)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataJson);
        };

        $verbose = fopen('php://temp', 'w+');
        if ($this->environment === 'dev.') {
            // Use http
            curl_setopt($ch, CURLOPT_URL, str_replace('https', 'http', $url));
            // to access local dev certificate
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            // change port locally
            curl_setopt($ch, CURLOPT_PORT, 80);

            // for debug purpose
            curl_setopt($ch, CURLOPT_VERBOSE, true);
            curl_setopt($ch, CURLOPT_STDERR, $verbose);
        }

        $resultClean = curl_exec($ch);
        $result = json_decode($resultClean);

        $info = curl_getinfo($ch);
        http_response_code($info['http_code']);

        switch($info['http_code']) {
            case 204:
                if ($this->debug) { $this->printLogs('API results 204 - empty response'); }
                if (isset($cacheResponse)) {
                    $this->apiRes[$endpoint] = $result;
                }
                http_response_code(200); // To avoid bug in Safari https://bugs.webkit.org/show_bug.cgi?id=60206
                break;
            case 200:
                if ($this->debug) { $this->printLogs('API results', $result); }
                if (isset($cacheResponse)) {
                    $this->apiRes[$endpoint] = $result;
                }

                // automatically save the userToken if found one
                $tmpUserIdToken = isset($result->userIdToken) ? $result->userIdToken : (isset($result->globalUserIdToken) ? $result->globalUserIdToken : ''); // TODO remove globalUserId
                if (!empty($tmpUserIdToken)) {
                    if ($this->debug) { $this->printLogs('Saving token coming from API'); }
                    $_SESSION[USER_ID_TOKEN] = $tmpUserIdToken;
                    if ($this->debug) { $this->printLogs('Token saved', $_SESSION[USER_ID_TOKEN]); }
                }

                // automatically save the metricsToken if found one
                if (!empty($result->metricsToken)) {
                    $_SESSION[$this->metricsTokenName] = $result->metricsToken;
                }
                if ($this->debug) { $this->printLogs('Call result OK'); }
            break;
            default:
                // advanced debug in case the error is unknown
                if ($resultClean === FALSE) {
                    printf("cUrl error (#%d): %s<br>\n", curl_errno($ch), htmlspecialchars(curl_error($ch)));

                    rewind($verbose);
                    $verboseLog = stream_get_contents($verbose);

                    echo "Verbose information:\n<pre>", htmlspecialchars($verboseLog), "</pre>\n";

                    echo 'Info<pre>';
                    var_dump($info);
                    echo '</pre>';
                    echo 'Result<pre>';
                    var_dump($result);
                    echo '</pre>';

                    trigger_error('Unknown, please contact SmartWall about this issue', E_USER_WARNING);
                    if ($this->debug) {
                        $this->printLogs('Unknow error calling API. '
                            .'cUrl error '.curl_errno($ch).': '.htmlspecialchars(curl_error($ch))
                            .'Verbose information: '.htmlspecialchars($verboseLog).'Info: ', $info);
                        $this->printLogs('Call result:', $result);
                    }
                } else {
                    // header('Content-type: application/json');
                    rewind($verbose);
                    $verboseLog = stream_get_contents($verbose);

                    if (!isset($res)) {
                        $result = new \stdClass();
                    }

                    $result->debug = array(
                        'headers' => $headers,
                        'data' => $dataJson,
                        'verbose' => $verboseLog
                    );
                    if ($this->debug) { $this->printLogs('Error calling API: ', $result); }
                    curl_close($ch);
                    return $result;
                }
            break;
        }
        curl_close($ch);
        return $result;
    }

    public static function urlsafeB64Decode($input) {
        $remainder = strlen($input) % 4;
        if ($remainder) {
            $padlen = 4 - $remainder;
            $input .= str_repeat('=', $padlen);
        }
        return base64_decode(strtr($input, '-_', '+/'));
    }

    public static function jsonDecode($input) {
        if (version_compare(PHP_VERSION, '5.4.0', '>=') && !(defined('JSON_C_VERSION') && PHP_INT_SIZE > 4)) {
            /** In PHP >=5.4.0, json_decode() accepts an options parameter, that allows you
             * to specify that large ints (like Steam Transaction IDs) should be treated as
             * strings, rather than the PHP default behaviour of converting them to floats.
             */
            $obj = json_decode($input, false, 512, JSON_BIGINT_AS_STRING);
        } else {
            /** Not all servers will support that, however, so for older versions we must
             * manually detect large ints in the JSON string and quote them (thus converting
             *them to strings) before decoding, hence the preg_replace() call.
             */
            $max_int_length = strlen((string) PHP_INT_MAX) - 1;
            $json_without_bigints = preg_replace('/:\s*(-?\d{'.$max_int_length.',})/', ': "$1"', $input);
            $obj = json_decode($json_without_bigints);
        }
        return $obj;
    }

    /**
    *  function to truncate and then clean up end of the HTML,
    *  truncates by counting characters outside of HTML tags
    *
    *  @author alex lockwood, alex dot lockwood at websightdesign
    *
    *  @param string $str the string to truncate
    *  @param int $len the number of characters
    *  @param string $end the end string for truncation
    *  @return string $truncated_html
    *
    *  **/
    private function truncateHTML($str, $len, $end = '&hellip;'){
        //find all tags
        $tagPattern = '/(<\/?)([\w]*)(\s*[^>]*)>?|&[\w#]+;/i';  //match html tags and entities
        preg_match_all($tagPattern, $str, $matches, PREG_OFFSET_CAPTURE | PREG_SET_ORDER );
        //WSDDebug::dump($matches); exit;
        $i =0;
        //loop through each found tag that is within the $len, add those characters to the len,
        //also track open and closed tags
        // $matches[$i][0] = the whole tag string  --the only applicable field for html enitities
        // IF its not matching an &htmlentity; the following apply
        // $matches[$i][1] = the start of the tag either '<' or '</'
        // $matches[$i][2] = the tag name
        // $matches[$i][3] = the end of the tag
        //$matces[$i][$j][0] = the string
        //$matces[$i][$j][1] = the str offest

        while($matches[$i][0][1] < $len && !empty($matches[$i])){

            $len = $len + strlen($matches[$i][0][0]);
            if(substr($matches[$i][0][0],0,1) == '&' )
            $len = $len-1;


            //if $matches[$i][2] is undefined then its an html entity, want to ignore those for tag counting
            //ignore empty/singleton tags for tag counting
            if(!empty($matches[$i][2][0]) && !in_array($matches[$i][2][0],array('br','img','hr', 'input', 'param', 'link'))){
                //double check
                if(substr($matches[$i][3][0],-1) !='/' && substr($matches[$i][1][0],-1) !='/')
                $openTags[] = $matches[$i][2][0];
                elseif(end($openTags) == $matches[$i][2][0]){
                    array_pop($openTags);
                }else{
                    $warnings[] = "html has some tags mismatched in it:  $str";
                }
            }


            $i++;

        }

        $closeTagString = '';

        if (!empty($openTags)){
            $openTags = array_reverse($openTags);
            foreach ($openTags as $t){
                $closeTagString .="</".$t . ">";
            }
        }

        if(strlen($str)>$len){
            // Finds the last space from the string new length
            $lastWord = strpos($str, ' ', $len);
            if ($lastWord) {
                //truncate with new len last word
                $str = substr($str, 0, $lastWord);
                //finds last character
                $last_character = (substr($str, -1, 1));
                //add the end text
                $truncated_html = ($last_character == '.' ? $str : ($last_character == ',' ? substr($str, 0, -1) : $str) . $end);
            }
            //restore any open tags
            $truncated_html .= $closeTagString;


        }else
        $truncated_html = $str;


        return $truncated_html;
    }

    /**
     * Print logs in stdout
     * @param string $msg message to print
     */
    private function printLogs($msg, $data=[]) {
        $stdout = fopen('php://stdout', 'w');
        $dateTime = date(DATE_RFC2822);
        $newLine = $dateTime.' '.$msg;
        if (!empty($data)) {
            $newLine = $newLine.' '.print_r($data, true);
        }
        fwrite($stdout, $newLine.PHP_EOL);
    }

    /**
     * Throws error and also logs it if debug mode is activated
     */
    private function throwError($description) {
        if ($this->debug) {
            $this->printLogs('Exception: '.$description);
        }
        throw new ErrorException($description);
    }
}

// Universal function for checking session status
function is_session_started()
{
    if ( php_sapi_name() !== 'cli' ) {
        if ( version_compare(phpversion(), '5.4.0', '>=') ) {
            return session_status() === PHP_SESSION_ACTIVE ? TRUE : FALSE;
        } else {
            return session_id() === '' ? FALSE : TRUE;
        }
    }
    return FALSE;
}

function checkMissingExtenssion() {
    $needed_extensions = array('curl');
    $missing_extensions = array();
    foreach ($needed_extensions as $needed_extension) {
        if (!extension_loaded($needed_extension)) {
            $missing_extensions[] = $needed_extension;
        }
    }
    if (count($missing_extensions) > 0) {
        $this->throwError('The SmartWall SDK needs the following extensions, please install/enable them: ' . implode(', ', $missing_extensions) . PHP_EOL);
    }
}

/**
* Helper library for CryptoJS AES encryption/decryption
* Allow you to use AES encryption on client side and server side vice versa
*
* @author BrainFooLong (bfldev.com)
* @link https://github.com/brainfoolong/cryptojs-aes-php
*/

/**
* Encrypt value to a cryptojs compatiable json encoding string
*
* @param mixed $passphrase
* @param mixed $value
* @return string
*/
function cryptoJsAesEncrypt($passphrase, $value){
    $salt = openssl_random_pseudo_bytes(8);
    $salted = '';
    $dx = '';
    while (strlen($salted) < 48) {
        $dx = md5($dx.$passphrase.$salt, true);
        $salted .= $dx;
    }
    $key = substr($salted, 0, 32);
    $iv  = substr($salted, 32,16);
    $encrypted_data = openssl_encrypt(json_encode($value), 'aes-256-cbc', $key, true, $iv);
    $data = array("ct" => base64_encode($encrypted_data), "iv" => bin2hex($iv), "s" => bin2hex($salt));
    return json_encode($data);
}
